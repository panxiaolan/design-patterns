package com.pxl.form.versionOne;

public class Teacher {
	private ExperimentCourse course;

	public void Set(ExperimentCourse course) {
		this.course = course;
	}

	public void Answer() {
		System.out.println(course);
	}
}

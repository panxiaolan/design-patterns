package com.pxl.form.versionTwo;

public class Application {
	public static void main(String[] args) {
		Teacher wang = new Teacher();
		
		System.out.println("今天上什么课？");
		Course c = new ExperimentCourse();
		// 通过依赖注入的方式注入到 Teacher 类中
		wang.Add(c);
		// 会调用其重写后的 toString() 方法
		wang.Answer();

		System.out.println("今天上什么课？");
		c = new TheoryCourse();
		// 只需要注入不同的课程即可,因为每门课都实现了 Course 接口,所以只需 new 不同的 课程即可
		wang.Add(c);
		wang.Answer();

		System.out.println("今天上什么课？");
		c = new DataBaseCourse();
		wang.Add(c);
		wang.Answer();

		System.out.println("哈哈，你可真...");
		System.out.println("多态发生在哪里？");
	}
}

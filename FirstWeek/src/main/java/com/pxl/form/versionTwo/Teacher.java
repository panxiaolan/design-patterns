package com.pxl.form.versionTwo;

public class Teacher {
	// 声明一个 Course 属性
	private Course course;
	// 实际上是通过依赖注入的方式,调用该方法是传入的是什么课程就赋值为什么课程
	public void Add(Course course){
		this.course = course;
	}
	public void Answer(){
		// 输出 对象的时候就会去调用默认的 toString 方法
		System.out.println(course);
	}
}

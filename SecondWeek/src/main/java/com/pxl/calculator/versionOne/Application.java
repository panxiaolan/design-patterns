package com.pxl.calculator.versionOne;

import java.util.Scanner;
public class Application {
	public static void main(String[] args) {
		int numberA,numberB;
		char oper = '+';
		double result=0;
		Scanner sc = new Scanner(System.in);

		System.out.println("请输入第一个参与运算的数numberA=：");
		numberA = sc.nextInt();
		System.out.println("请输入第二个参与运算的数numberB=：");
		numberB = sc.nextInt();
		System.out.print("请选择参与运算的类型(+、-、*、/):");
		oper = (sc.next()).charAt(0);

		// 每次都需要进行手动创建运算符对象,太过麻烦,而且如果运算符较多
		// 代码冗余沉重
		
		Add a = new Add(numberA,numberB);
		Sub s = new Sub(numberA,numberB);
		Mul m = new Mul(numberA,numberB);
		Div d = new Div(numberA,numberB);

		switch (oper) {
			case '+': result = a.GetResult();break;
			case '-': result = s.GetResult();break;
			case '*': result = m.GetResult();break;
			case '/': result = d.GetResult();break;
		}
		System.out.println("numberA" + oper + "numberB" + " = " + Double.toString(result));
}
}

package com.pxl.calculator.versionOne;

public class Div {
	private int num1;
	private int num2;
	public Div(int num1,int num2){
		this.num1 = num1;
		this.num2 = num2;
	}
	public int GetResult(){
		return num1 / num2;
	}
}
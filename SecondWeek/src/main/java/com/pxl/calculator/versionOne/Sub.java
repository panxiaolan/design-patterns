package com.pxl.calculator.versionOne;

public class Sub {
	private int num1;
	private int num2;
	public Sub(int num1,int num2){
		this.num1 = num1;
		this.num2 = num2;
	}
	public int GetResult(){
		return num1 - num2;
	}
}

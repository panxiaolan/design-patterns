package com.pxl.calculator.versionTwo;

import java.util.Scanner;
public class Application {
	public static void main(String[] args) {
		int numberA,numberB;
		char oper = '+';
		double result=0;
		Scanner sc = new Scanner(System.in);

		System.out.println("请输入第一个参与运算的数numberA=：");
		numberA = sc.nextInt();
		System.out.println("请输入第二个参与运算的数numberB=：");
		numberB = sc.nextInt();
		System.out.print("请选择参与运算的类型(+、-、*、/):");


		oper = (sc.next()).charAt(0);

		// new 一个操作符的工厂,我们将目标对象传递给工厂,交给工厂来做
		OperatorFactory oFactory = new OperatorFactory();
		// 通过工厂调用其方法,进行运算
		Operator o = oFactory.GetOperator(oper);

		if (o != null)
			result = o.GetResult(numberA,numberB);
		System.out.println("numberA" + oper + "numberB" + " = " + Double.toString(result));
	}
}

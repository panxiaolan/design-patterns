package com.pxl.calculator.versionTwo;

public interface Operator
{
	public abstract int GetResult(int num1, int num2);
}

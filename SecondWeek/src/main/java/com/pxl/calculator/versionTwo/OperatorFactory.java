package com.pxl.calculator.versionTwo;

public class OperatorFactory {
	public Operator GetOperator(char str){
		Operator o = null;
		switch (str){
			case '+':o = new Add();break;
			case '-':o = new Sub();break;
			case '*':o = new Mul();break;
			case '/':o = new Div();break;
		}
		return o;
	}
}